import { Component } from "@angular/core";

@Component ({
    selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html'
})


export class HotelListComponent {

    public title='Liste Hotels';

    public hotels: any[] = [
      {
        hotelId: 1,
        hotelName: 'Buea sweet life',
        description: 'Belle vue au bord de la mer',
        price: 230.5,
        imageUrl: 'assets/img/hotel-room.jpg'
      },
      {
        hotelId: 2,
        hotelName: 'Marakech',
        description: 'profitez de la vue sur la montagne',
        price: 145.5,
        imageUrl: 'assets/img/the-interior.jpg'
      },

      {
        hotelId: 3,
        hotelName: 'Buea sweet life',
        description: 'Belle vue au bord de la mer',
        price: 120.5,
        imageUrl: 'assets/img/indoors.jpg'
      },

      {
        hotelId: 4,
        hotelName: 'Cape town city',
        description: 'magnifique cadre pour votre séjour',
        price: 230.5,
        imageUrl: 'assets/img/window.jpg'
      }

    ];

    public showBadge: boolean;

    public hotelFilter='mot';


    public toggleIsNewBadge(): void{
      this.showBadge = !this.showBadge;
    }

}